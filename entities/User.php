<?php
// class entities\User - represents "users" table of DB

namespace entities;

class User {
    public $id;
    public $email;
    public $password;
    public $access_token;
    public $photo;
    public $fio;
    public $password_reset_token;

    // main constructor to fill class parameters
    function __construct($id, $email, $password, $access_token, $photo, $fio, $password_reset_token) {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->access_token = $access_token;
        $this->photo = $photo;
        $this->fio = $fio;
        $this->password_reset_token = $password_reset_token;
    }
}