<?php
// main and only DB class - opens mysql connection and returns $mysqli object if no error, if it is - displays error
// NOTE: script gets mysql connect params from config class

require_once dirname(__FILE__) . '/../config.php';

class DB {

    public static function initConnection() {
        $mysqli = new mysqli(config::MYSQL_HOST, config::MYSQL_USER, config::MYSQL_PASSWORD, config::MYSQL_DB);
        if ($mysqli->connect_errno) {
            echo "Can't connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
            die();
        }
        $mysqli->set_charset("utf8");

        return $mysqli;
    }
}