<?php
// Main component class - contains all useful methods, used more than once.

require_once dirname(__FILE__) . '/../config.php';
require_once APP_PATH . 'components/DB.php';
require_once APP_PATH . 'entities/User.php';
require_once APP_PATH . 'models/Response.php';

class Util {

    // checks access token cookie: if it exists - return user that has this token, if no - returns to the main page
    public static function checkAccess() {
        $user = null;
        if (isset($_COOKIE['accessToken']) && !empty($_COOKIE['accessToken'])) {
            $user = self::getUserByToken($_COOKIE['accessToken']);
        }
        if ($user == null) {
            self::doInternalRedirect('index.php');
            die();
        }
        return $user;
    }

    // checks password reset token: if it exists - return user that has this token, if no - return to the main page with some hash
    // - so JS can detect it and show notification
    public static function checkPasswordResetToken() {
        $user = null;
        if (isset($_GET['token']) && !empty($_GET['token'])) {
            $user = self::getUserByPasswordResetToken($_GET['token']);
        }
        if ($user == null) {
            self::doInternalRedirect('index.php#passwordResetTokenError');
            die();
        }
        return $user;
    }

    // redirects to the specific page of site, recieves relative path and add it to the base site url
    public static function doInternalRedirect($path) {
        header('Location: ' . self::getSiteUrl() . $path);
    }

    // returns base sate url depending on HTTP_ORIGIN and configured site name, so we can easily change site path
    public static function getSiteUrl() {
        return $_SERVER['HTTP_ORIGIN'] . '/'. config::SITE_NAME . '/';
    }

    // generate md5 token depending on user ID and current unix time, so it's must be unique
    public static function generateToken($userId) {
        return md5($userId . time());
    }

    // generate file name using md5 depending current unix time, so it's must be unique
    public static function generateFilename($filename) {
        return md5($filename. time()) . '.' . pathinfo($filename, PATHINFO_EXTENSION);
    }

    // init response data. This method must be used to return stadartized json
    // recieves data and errors array
    public static function initResponse($data, $errors = array()) {
        $response = new \models\Response();

        $response->errors = $errors;
        if (count($response->errors) == 0) { // if no errors
            $response->data = $data; // setting data
        }

        return $response;
    }

    // get user entity from DB by email
    public static function getUserByEmail($email) {
        $mysqli = DB::initConnection();

        // doing select from DB
        $stmt = $mysqli->prepare("SELECT * FROM users WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->bind_result($id, $email, $password, $accessToken, $photo, $fio, $password_reset_token);
        $stmt->fetch();
        $stmt->close();

        $mysqli->close();

        if ($id > 0) { // if user exists
            // init user entity and fills it with values
            $user = new \entities\User($id, $email, $password, $accessToken, $photo, $fio, $password_reset_token);
            return $user;
        } else {
            return null;
        }
    }

    // get user entity from DB by email and password
    public static function getUserByEmailAndPassword($email, $password) {
        $mysqli = DB::initConnection();

        // doing select from DB
        $stmt = $mysqli->prepare("SELECT * FROM users WHERE email = ? and password = ?");
        $stmt->bind_param("ss", $email, $password);
        $stmt->execute();
        $stmt->bind_result($id, $email, $password, $accessToken, $photo, $fio, $password_reset_token);
        $stmt->fetch();
        $stmt->close();

        $mysqli->close();

        if ($id > 0) { // if user exists
            // init user entity and fills it with values
            $user = new \entities\User($id, $email, $password, $accessToken, $photo, $fio, $password_reset_token);
            return $user;
        } else {
            return null;
        }
    }

    // get user entity from DB by password reset token
    public static function getUserByPasswordResetToken($token) {
        $mysqli = DB::initConnection();

        // doing select from DB
        $stmt = $mysqli->prepare("SELECT * FROM users WHERE password_reset_token = ?");
        $stmt->bind_param("s", $token);
        $stmt->execute();
        $stmt->bind_result($id, $email, $password, $accessToken, $photo, $fio, $password_reset_token);
        $stmt->fetch();
        $stmt->close();

        $mysqli->close();

        if ($id > 0) { // if user exists
            // init user entity and fills it with values
            $user = new \entities\User($id, $email, $password, $accessToken, $photo, $fio, $password_reset_token);
            return $user;
        } else {
            return null;
        }
    }

    // get user entity from DB by access token
    private function getUserByToken($token) {
        $mysqli = DB::initConnection();

        // doing select from DB
        $stmt = $mysqli->prepare("SELECT * FROM users WHERE access_token = ?");
        $stmt->bind_param("s", $token);
        $stmt->execute();
        $stmt->bind_result($id, $email, $password, $accessToken, $photo, $fio, $password_reset_token);
        $stmt->fetch();
        $stmt->close();

        $mysqli->close();

        if ($id > 0) { // if user exists
            // init user entity and fills it with values
            $user = new \entities\User($id, $email, $password, $accessToken, $photo, $fio, $password_reset_token);
            return $user;
        } else {
            return null;
        }
    }
}