<?php
// main and only language changing class

require_once dirname(__FILE__) . '/../config.php';

class Lang {

    private static $languageData;

    // get current language code depending on cookie, if it not exists - returns 'ru' as default language
    private function getCurrentLanguage() {
        return (isset($_COOKIE['lang']) ? $_COOKIE['lang'] : 'ru');
    }

    // get language data (array of words from json language file)
    public static function getLanguageData() {
        // for more performance check if we already have filled $languageData property
        if (!isset(self::$languageData)) {
            $jsonString = file_get_contents(APP_PATH . 'resources/lang/' . self::getCurrentLanguage() . '.json');
            self::$languageData = json_decode($jsonString, true);
        }

        return self::$languageData;
    }

    // rendering language selector (used once at the main page)
    // NOTE: method gets available languages from main config class
    public static function renderLangSelector() {
        $html = '';
        foreach (config::AVAILABLE_LANGS as $key => $value) {
            if ($value == self::getCurrentLanguage()) {
                $html .= $value . ' | ';
            } else {
                $html .= '<a onclick="changeLanguage(this)" href="javascript:void(0)">' . $value . '</a> | ';
            }
        }

        echo $html;
    }
}