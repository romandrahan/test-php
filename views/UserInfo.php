<?php

require_once APP_PATH . 'components/Util.php';

$user = Util::checkAccess();

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?=Lang::getLanguageData()['dom']['userInfoTitle']?></h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="form-group">
                <div class="col-sm-12" id="user-photo-container">
                    <img src="<?=config::UPLOAD_PATH . $user->photo?>" class="img-thumbnail">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label"><?=Lang::getLanguageData()['dom']['email']?></label>
                <div class="col-sm-8">
                    <p class="form-control-static"><?=$user->email?></p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label"><?=Lang::getLanguageData()['dom']['fio']?></label>
                <div class="col-sm-8">
                    <p class="form-control-static"><?=$user->fio?></p>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <a href="actions/Logout.php" class="btn btn-default center-block">
                        <?=Lang::getLanguageData()['dom']['logoutBtn']?>
                    </a>
                </div>
            </div>
        </form>
    </div>
</div>