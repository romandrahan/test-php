<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?=Lang::getLanguageData()['dom']['signInTitle']?></h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" method="post" action="actions/SignIn.php" novalidate>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=Lang::getLanguageData()['dom']['email']?></label>
                <div class="col-sm-9">
                    <input type="email" name="email" class="form-control" autofocus>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label"><?=Lang::getLanguageData()['dom']['password']?></label>
                <div class="col-sm-9">
                    <input type="password" name="password" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <button class="btn btn-primary btn-block" type="submit">
                        <?=Lang::getLanguageData()['dom']['signInBtn']?>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-default footer">
    <div class="panel-body">
        <div class="col-sm-6 text-center">
            <a href="index.php?view=SignUpForm"><?=Lang::getLanguageData()['dom']['signUpLink']?></a>
        </div>
        <div class="col-sm-6 text-center">
            <a href="index.php?view=ForgetPasswordForm"><?=Lang::getLanguageData()['dom']['forgetPasswordLink']?></a>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('form').submit(function() { // on form submit

        // defining validation rules
        var rules = [
            { targets : [$('input[name=email]')], type : 'notEmpty', messageId : 'email' },
            { targets : [$('input[name=password]')], type : 'notEmpty', messageId : 'password' }
        ]

        // validate
        validateBootstrapForm(this, rules, function(form) { // if validations is success
            $(form).ajaxSubmit({ // submit form
                success: function (responseText, statusText, xhr, $form) {
                    var response = JSON.parse(responseText)

                    if (response.errors.length == 0) { // if no errors
                        // create user cookie and doing redirect to the user profile
                        $.cookie('accessToken', response.data, {path: '/'})
                        doInternalRedirect('index.php?view=UserInfo')
                    } else { // if we have errors from server - show them
                        showWarningMessages(response.errors)
                    }
                }
            })
        })

        return false
    })
</script>