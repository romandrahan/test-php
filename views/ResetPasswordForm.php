<?php

require_once APP_PATH . 'components/Util.php';

$user = Util::checkPasswordResetToken();

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?=Lang::getLanguageData()['dom']['resetPasswordTitle']?></h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" method="post" action="actions/ResetPassword.php" novalidate>
            <input type="hidden" name="passwordResetToken" value="<?=$user->password_reset_token?>">

            <div class="form-group">
                <label class="col-sm-5 control-label"><?=Lang::getLanguageData()['dom']['password']?></label>
                <div class="col-sm-7">
                    <input type="password" name="password" class="form-control" autofocus>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-5 control-label"><?=Lang::getLanguageData()['dom']['passwordAgain']?></label>
                <div class="col-sm-7">
                    <input type="password" name="passwordAgain" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <button class="btn btn-primary btn-block" type="submit">
                        <?=Lang::getLanguageData()['dom']['saveBtn']?>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $('form').submit(function() { // on form submit

        // defining validation rules
        var rules = [
            { targets : [$('input[name=password]')], type : 'notEmpty', messageId : 'password' },
            { targets : [$('input[name=passwordAgain]')], type : 'notEmpty', messageId : 'passwordAgain' },
            { targets : [$('input[name=password]'), $('input[name=passwordAgain]')], type : 'targetsNotMatch', messageId : 'passwordsNotMatch' }
        ]

        // validate
        validateBootstrapForm(this, rules, function(form) { // if validations is success
            $(form).ajaxSubmit({ // submit form
                success: function(responseText, statusText, xhr, $form) {
                    var response = JSON.parse(responseText)

                    if (response.errors.length == 0) { // if no errors
                        // create user cookie and doing redirect to the user profile
                        $.cookie("accessToken", response.data, { path: '/' })
                        doInternalRedirect('index.php?view=UserInfo')
                    } else { // if we have errors from server - show them
                        showWarningMessages(response.errors)
                    }
                }
            })
        })

        return false
    })
</script>