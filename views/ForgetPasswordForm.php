<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?=Lang::getLanguageData()['dom']['forgetPasswordTitle']?></h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" method="post" action="actions/ForgetPassword.php" novalidate>
            <div class="form-group">
                <label class="col-sm-2 control-label"><?=Lang::getLanguageData()['dom']['email']?></label>
                <div class="col-sm-10">
                    <input type="email" name="email" class="form-control" autofocus>
                    <p class="help-block"><?=Lang::getLanguageData()['dom']['forgetPasswordHelpText']?></p>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <button class="btn btn-primary btn-block" type="submit">
                        <?=Lang::getLanguageData()['dom']['sendBtn']?>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-default footer">
    <div class="panel-body">
        <div class="form-group">
            <div class="col-sm-6 text-center">
                <a href="index.php?view=SignInForm"><?=Lang::getLanguageData()['dom']['signInLink']?></a>
            </div>
            <div class="col-sm-6 text-center">
                <a href="index.php?view=SignUpForm"><?=Lang::getLanguageData()['dom']['signUpLink']?></a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('form').submit(function() { // on form submit

        // defining validation rules
        var rules = [
            { targets : [$('input[name=email]')], type : 'notEmpty', messageId : 'email' }
        ]

        // validate
        validateBootstrapForm(this, rules, function(form) { // if validations is success
            $(form).ajaxSubmit({ // submit form
                success: function(responseText, statusText, xhr, $form) {
                    var response = JSON.parse(responseText)

                    if (response.errors.length == 0) { // if no errors
                        // show info message and doing redirect to the main page
                        showInformationMessage('<?=Lang::getLanguageData()['infoMessages']['forgetPasswordMailSent']?>',
                            5000, function() {
                                doInternalRedirect('index.php')
                            }
                        )
                    } else { // if we have errors from server - show them
                        showWarningMessages(response.errors)
                    }
                }
            })
        })

        return false
    })
</script>