<?php

namespace forms;

require_once APP_PATH . 'models/forms/AbstractForm.php';
require_once APP_PATH . 'models/forms/Form.php';
require_once APP_PATH . 'components/Lang.php';

use models\Error;

class ForgetPasswordForm extends AbstractForm implements Form {

    public $email;

    public function validate() {
        if (empty($this->email) || !filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $this->errors[] = new Error(2, \Lang::getLanguageData()['validationMessages']['email']);
        }

        return count($this->errors) == 0;
    }

    public function loadFromRequest() {
        $this->email = $_POST['email'];
    }
}