<?php

namespace forms;

require_once APP_PATH . 'models/forms/AbstractForm.php';
require_once APP_PATH . 'models/forms/Form.php';
require_once APP_PATH . 'components/Lang.php';

use models\Error;

class SignUpForm extends AbstractForm implements Form {

    public $email;
    public $password;
    public $passwordAgain;
    public $fio;
    public $photo;

    public function validate() {
        if (empty($this->email) || !filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $this->errors[] = new Error(2, \Lang::getLanguageData()['validationMessages']['email']);
        }

        if (empty($this->password)) {
            $this->errors[] = new Error(2, \Lang::getLanguageData()['validationMessages']['password']);
        }

        if (empty($this->passwordAgain)) {
            $this->errors[] = new Error(2, \Lang::getLanguageData()['validationMessages']['passwordAgain']);
        }

        if ($this->password != $this->passwordAgain) {
            $this->errors[] = new Error(2, \Lang::getLanguageData()['validationMessages']['passwordsNotMatch']);
        }

        if (empty($this->fio)) {
            $this->errors[] = new Error(2, \Lang::getLanguageData()['validationMessages']['fio']);
        }

        if (isset($this->photo)) {
            // Allow certain file formats
            $fileExtension = pathinfo($this->photo['name'], PATHINFO_EXTENSION);
            $check = getimagesize($_FILES["photo"]["tmp_name"]);
            if ($check === false || ($fileExtension != 'jpg' && $fileExtension != 'png' && $fileExtension != 'jpeg'
                    && $fileExtension != 'gif')) {
                $this->errors[] = new Error(2, \Lang::getLanguageData()['validationMessages']['wrongPhotoFormat']);
            }
        }

        return count($this->errors) == 0;
    }

    public function loadFromRequest() {
        $this->email = $_POST['email'];
        if (!empty($_POST['password'])) {
            $this->password = md5($_POST['password']);
        }
        if (!empty($_POST['passwordAgain'])) {
            $this->passwordAgain = md5($_POST['passwordAgain']);
        }
        $this->fio = $_POST['fio'];

        if (isset($_FILES['photo'])) {
            $this->photo = $_FILES['photo'];
        }
    }
}