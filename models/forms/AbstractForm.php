<?php
// forms\AbstractForm class - generic class of general typical app html form,
// must be extended by all forms to simplfiy working with fields and validations

namespace forms;

class AbstractForm {

    public $errors = array();

    function __construct() {
        // loading all fields from GET, POST, FILES variables
        // (each child class has own implementation of loadFromRequest method of course)
        $this->loadFromRequest();
    }
}