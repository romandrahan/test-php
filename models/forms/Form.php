<?php
// forms\Form interface - ingerface of general typical app html form,
// must be implemented by all forms to simplfiy working with fields and validations

namespace forms;

interface Form {

    public function validate();
    public function loadFromRequest();
}