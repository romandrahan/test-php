<?php

namespace forms;

require_once APP_PATH . 'models/forms/AbstractForm.php';
require_once APP_PATH . 'models/forms/Form.php';
require_once APP_PATH . 'components/Lang.php';

use models\Error;

class ResetPasswordForm extends AbstractForm implements Form {

    public $password;
    public $passwordAgain;
    public $passwordResetToken;

    public function validate() {
        if (empty($this->password)) {
            $this->errors[] = new Error(2, \Lang::getLanguageData()['validationMessages']['password']);
        }

        if (empty($this->passwordAgain)) {
            $this->errors[] = new Error(2, \Lang::getLanguageData()['validationMessages']['passwordAgain']);
        }

        if ($this->password != $this->passwordAgain) {
            $this->errors[] = new Error(2, \Lang::getLanguageData()['validationMessages']['passwordsNotMatch']);
        }

        return count($this->errors) == 0;
    }

    public function loadFromRequest() {
        if (!empty($_POST['password'])) {
            $this->password = md5($_POST['password']);
        }
        if (!empty($_POST['passwordAgain'])) {
            $this->passwordAgain = md5($_POST['passwordAgain']);
        }

        $this->passwordResetToken = $_POST['passwordResetToken'];
    }
}