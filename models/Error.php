<?php
// models\Error class - generic class of all app errors, need to be used when returning some validation or other errors from actions

namespace models;

class Error {
    public $type; // 1 - general error/warning, 2 - validation error
    public $text; // error text to show

    function __construct($type, $text) {
        $this->type = $type;
        $this->text = $text;
    }
}