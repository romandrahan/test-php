<?php
// models\Response class - generic class for all json responses from app,
// need to be used to standartize all responses and make integration with JS layer more simple

namespace models;

class Response {
    public $data = null; // data needed to return
    public $errors = array(); // array of \models\Error objects
}