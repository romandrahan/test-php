<?php
// main config file - all server settings goes here

define('APP_PATH', dirname(__FILE__) . '/');

class config {

    const MYSQL_HOST =          'localhost';
    const MYSQL_USER =          'root';
    const MYSQL_PASSWORD =      'masterkey';
    const MYSQL_DB =            'test-php-dragan';
    const SITE_NAME =           'test-php';
    const UPLOAD_PATH =         'uploads/';
    const WEBMASTER_EMAIL =     'roman@dragan.com.ua';
    const DEFAULT_USER_PHOTO =  'default.jpg';
    const AVAILABLE_LANGS =     array('ru', 'en'); // work only since PHP 5.6
}