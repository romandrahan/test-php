var SITE_BASE_PATH = 'test-php'
var USE_JS_VALIDATION = true

$.noty.defaults.layout = 'topRight'
$.noty.defaults.theme = 'relax' //'defaultTheme', // or 'relax'
$.noty.defaults.animation = {
    open: 'animated flipInX',
    close: 'animated flipOutX'
}

if ($.cookie('lang') === undefined) {
    $.cookie('lang', 'ru', { path: '/' })
}

function changeLanguage(target) {
    $.cookie('lang', $(target).text().trim(), { path: '/' })
    window.location.reload();
}

function doInternalRedirect(path) {
    window.location.href = window.location.origin + '/' + SITE_BASE_PATH + '/' + path
}

function showWarningMessages(errors) {
    $.noty.closeAll()

    $.each(errors, function(key, value) {
        var notyType

        if (value.type == 1) {
            notyType = 'warning'
        } else if (value.type == 2) {
            notyType = 'alert'
        }

        noty({
            text: value.text,
            type: notyType
        })
    })
}

function showInformationMessage(text, timeout, onCLoseCallback) {
    $.noty.closeAll()

    var defaultCallback = $.noty.defaults.callback
    var defaultTimeout = $.noty.defaults.timeout

    if (onCLoseCallback !== undefined && onCLoseCallback != null) {
        defaultCallback.onClose = onCLoseCallback
    }

    if (timeout !== undefined && timeout != null) {
        defaultTimeout = timeout
    }

    noty({
        text: text,
        type: 'information',
        //layout: 'center',
        callback: defaultCallback,
        timeout: defaultTimeout
    })
}

function validateBootstrapForm(targetForm, rules, successCallback) {
    if (!USE_JS_VALIDATION) {
        successCallback(targetForm)
        return
    }

    var errorClass = 'has-error'
    $('.form-group').removeClass(errorClass)

    $.getJSON('resources/lang/' + $.cookie('lang') + '.json', function(languageData) {
        var errors = []
        $.each(rules, function (key, rule) {
            if (rule.type == 'notEmpty') {
                $.each(rule.targets, function (targetKey, targetValue) {
                    if ($(targetValue).val() == '' ||
                        ($(targetValue).attr('type') == 'email' && !validateEmail($(targetValue).val()))) {
                        errors.push({type: 2, text: languageData['validationMessages'][rule.messageId]})
                        $(targetValue).closest('.form-group').addClass(errorClass)
                    }
                })
            }
            if (rule.type == 'targetsNotMatch') {
                if (rule.targets.length == 2 && $(rule.targets[0]).val() != $(rule.targets[1]).val()) {
                    errors.push({type: 2, text: languageData['validationMessages'][rule.messageId]})
                    $(rule.targets[0]).closest('.form-group').addClass(errorClass)
                    $(rule.targets[1]).closest('.form-group').addClass(errorClass)
                }
            }
        })

        showWarningMessages(errors)

        if (errors.length == 0) {
            successCallback(targetForm)
        }
    })
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    return re.test(email)
}