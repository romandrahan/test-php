<?php
// actions/ResetPassword.php action - checks password reset token in DB, resets password to the new from user input

require_once dirname(__FILE__) . '/../config.php';
require_once APP_PATH . 'components/Util.php';
require_once APP_PATH . 'models/Error.php';
require_once APP_PATH . 'models/forms/ResetPasswordForm.php';
require_once APP_PATH . 'components/Lang.php';

$errors = array();
// init form depending on POST params
$form = new \forms\ResetPasswordForm();
if (!$form->validate()) { // validating form fields
    echo json_encode(Util::initResponse(null, $form->errors)); // return json string with errors
    die();
}

$user = Util::getUserByPasswordResetToken($form->passwordResetToken); // getting user by recieved password reset token

if ($user == null) { // if user does not exist in DB
    // return json string with error
    $errors[] = new \models\Error(1, Lang::getLanguageData()['errorMessages']['resetPasswordError']);
    echo json_encode(Util::initResponse(null, $errors));
    die();
}
// updating model fields ($form->password already contains md5() encoded password)
$user->password = $form->password;
$user->password_reset_token = '';

$mysqli = DB::initConnection();
// storing new password in DB
$stmt = $mysqli->prepare("UPDATE users SET password = ?, password_reset_token = ?  WHERE id = ?");
$stmt->bind_param("ssi", $user->password, $user->password_reset_token, intval($user->id));
$stmt->execute();

$stmt->close();
$mysqli->close();

echo json_encode(Util::initResponse($user->access_token)); // return access token. so JS can auto login the user