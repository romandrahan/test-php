<?php
// actions/ForgetPassword.php action - checks user email in DB, generates password reset url, sends Email to user

require_once dirname(__FILE__) . '/../config.php';
require_once APP_PATH . 'components/Util.php';
require_once APP_PATH . 'models/Error.php';
require_once APP_PATH . 'models/forms/ForgetPasswordForm.php';
require_once APP_PATH . 'components/Lang.php';

$errors = array();
// init form depending on POST params
$form = new \forms\ForgetPasswordForm();
if (!$form->validate()) { // validating form fields
    echo json_encode(Util::initResponse(null, $form->errors)); // return json string with errors
    die();
}

$user = Util::getUserByEmail($form->email); // getting user by recieved email

if ($user == null) { // if user does not exist in DB
    // return json string with error
    $errors[] = new \models\Error(1, Lang::getLanguageData()['errorMessages']['userWithEmailNotExists']);
    echo json_encode(Util::initResponse(null, $errors));
    die();
}
// if user exists
$user->password_reset_token = Util::generateToken($user->id); // generate password reset token depending on user ID

$mysqli = DB::initConnection();
// storing password reset token to DB
$stmt = $mysqli->prepare("UPDATE users SET password_reset_token = ?  WHERE id = ?");
$stmt->bind_param("si", $user->password_reset_token, intval($user->id));
$stmt->execute();

$stmt->close();
$mysqli->close();

// generating mail subject and text depending on template and language
$mailText = str_replace('#{resetPasswordUrl}',
    Util::getSiteUrl() . 'index.php?view=ResetPasswordForm&token=' . $user->password_reset_token,
    Lang::getLanguageData()['emailTemplates']['forgetPassword']['text']);
$mailSubject = str_replace('#{siteName}',
    config::SITE_NAME,
    Lang::getLanguageData()['emailTemplates']['forgetPassword']['subject']);
$result = mail($user->email, $mailSubject, $mailText); // sending mail with password reset url to user Email

//echo json_encode(Util::initResponse($mailText)); // for development return mail text
echo json_encode(Util::initResponse($result)); // for production return only mail() function result