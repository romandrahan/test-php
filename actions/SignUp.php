<?php
// actions/SignUp.php action - checks if recieved email not already exists in DB, if no - create user and returns his generated access token

require_once dirname(__FILE__) . '/../config.php';
require_once APP_PATH . 'components/DB.php';
require_once APP_PATH . 'components/Util.php';
require_once APP_PATH . 'models/Error.php';
require_once APP_PATH . 'models/forms/SignUpForm.php';
require_once APP_PATH . 'components/Lang.php';

$errors = array();
// init form depending on POST params
$signUpForm = new \forms\SignUpForm();
if (!$signUpForm->validate()) { // validating form fields
    echo json_encode(Util::initResponse(null, $signUpForm->errors)); // return json string with errors
    die();
}

$user = Util::getUserByEmail($signUpForm->email); // getting user by recieved email
if ($user != null) {
    $errors[] = new \models\Error(1, Lang::getLanguageData()['errorMessages']['userWithEmailExists']);
    echo json_encode(Util::initResponse(null, $errors));
    die();
}

// Generating unique filename and do upload
$photo = Util::generateFilename($signUpForm->photo["name"]);
if (!move_uploaded_file($signUpForm->photo["tmp_name"], __DIR__ . '/../' . config::UPLOAD_PATH . $photo)) { // if no error while moving file
    $photo = config::DEFAULT_USER_PHOTO;
}

$mysqli = DB::initConnection();
// storing new user to DB
$stmt = $mysqli->prepare("INSERT INTO users (email, password, photo, fio) VALUES (?, ?, ?, ?)");
$stmt->bind_param("ssss", $signUpForm->email, $signUpForm->password, $photo, $signUpForm->fio);
$stmt->execute();
$newId = $stmt->insert_id;
$stmt->close();
// generating new user access token depending on user ID
$accessToken = Util::generateToken($newId);
// storing new access token to DB
$stmt = $mysqli->prepare("UPDATE users SET access_token = ? WHERE id = ?");
$stmt->bind_param("si", $accessToken, intval($newId));
$stmt->execute();
$stmt->close();

$mysqli->close();

echo json_encode(Util::initResponse($accessToken)); // return access token so JS can create cookie and do auto login