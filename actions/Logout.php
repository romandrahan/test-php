<?php
// actions/Logout.php action - removes token cookie, does redirect to the main page

require_once dirname(__FILE__) . '/../config.php';
require_once APP_PATH . 'components/Util.php';

setcookie('accessToken', '', time() - 3600);
Util::doInternalRedirect('index.php');