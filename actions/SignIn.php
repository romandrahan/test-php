<?php
// actions/SignIn.php action - checks user email and password, returns access token if success, or returns errors if check fails

require_once dirname(__FILE__) . '/../config.php';
require_once APP_PATH . 'components/DB.php';
require_once APP_PATH . 'components/Util.php';
require_once APP_PATH . 'models/Error.php';
require_once APP_PATH . 'models/forms/SignInForm.php';
require_once APP_PATH . 'components/Lang.php';

$errors = array();
// init form depending on POST params
$signInForm = new \forms\SignInForm();
if (!$signInForm->validate()) { // validating form fields
    echo json_encode(Util::initResponse(null, $signInForm->errors)); // return json string with errors
    die();
}
// getting user by recieved email and password
$user = Util::getUserByEmailAndPassword($signInForm->email, $signInForm->password);
if ($user == null) { // if user does not exist in DB
    // return json string with error
    $errors[] = new \models\Error(1, Lang::getLanguageData()['errorMessages']['wrongEmailPassword']);
    echo json_encode(Util::initResponse(null, $errors));
    die();
}

if (empty($user->access_token)) { // if user access token is empty in DB
    // generate new access token
    $user->access_token = Util::generateToken($user->id);

    $mysqli = DB::initConnection();
    // storing new token to DB
    $stmt = $mysqli->prepare("UPDATE users SET access_token = ? WHERE id = ?");
    $stmt->bind_param("si", $user->access_token, intval($user->id));
    $stmt->execute();

    $stmt->close();
    $mysqli->close();
}

echo json_encode(Util::initResponse($user->access_token)); // return access token so JS can create cookie and login user