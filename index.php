<?php

require_once 'config.php';
require_once APP_PATH . 'components/Lang.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?=Lang::getLanguageData()['dom']['title']?></title>

    <!-- Bootstrap CSS -->
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="resources/css/animate.css" rel="stylesheet">
    <!-- User global CSS -->
    <link href="resources/css/main.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="resources/js/jquery-1.11.3.min.js"></script>
    <!-- jQuery Cookie plugin -->
    <script src="resources/js/jquery.cookie.js"></script>
    <!-- jQuery ajax form plugin -->
    <script src="resources/js/jquery.form.min.js"></script>
    <!-- jQuery Noty plugin -->
    <script src="resources/js/jquery.noty.packaged.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="resources/js/bootstrap.min.js"></script>
    <!-- User global JS -->
    <script src="resources/js/main.js"></script>
</head>
<body>
    <div class="container">
        <div id="change-lang-container" class="pull-right">
            <?php Lang::renderLangSelector(); ?>
        </div>
        <?php
        // renders partial view depending on what we get in GET[veiw] param
        include 'views/' . (isset($_GET["view"]) && !empty($_GET["view"]) ? $_GET["view"] : 'SignInForm') . '.php';
        ?>
    </div>
    <!-- User document ready -->
    <script type="text/javascript">
        $(function () {
            if (document.location.hash == '#passwordResetTokenError') {
                showWarningMessages([
                    { type : 1, text : '<?=Lang::getLanguageData()['errorMessages']['passwordResetTokenError']?>' }
                ])
            }
        })
    </script>
</body>
</html>